import { combineReducers } from 'redux';
import login from 'reducers/login';
import register from 'reducers/register';
import car from 'reducers/pool';

export default combineReducers({
  login,
  register,
  car
});
