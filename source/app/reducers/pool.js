import { POOL } from '../lib/constant';

const initialState = {car:{}};

export default function(state = initialState , action ) {
	switch(action.type){
		case POOL :
			return {...state,car:action.payload};
        default :
		     return state;
    }
}
