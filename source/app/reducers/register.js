import { REGISTER } from '../lib/constant';

const initialState = {register:{}};

export default function(state = initialState , action ) {
	switch(action.type){
		case REGISTER :
			return {...state,register:action.payload};
        default :
		     return state;
    }
}
