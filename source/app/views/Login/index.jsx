import React, { Component } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

import MenuBar from '../MenuBar';
import {loginReq} from '../../actions/login';
import {mobileValidator,emailValidator,setCookie,getCookie} from '../../lib/utils';

class Login extends Component {
    constructor(){
        super();
        this.state = {
            userIdMobile : '',
            password : '',
            err : null
        };
    }
    componentWillMount(){
        if(getCookie("userId")){
            this.context.router.history.push("/pool");
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.login.success){
            setCookie("userId",nextProps.login.data.userId, 1);
            this.context.router.history.push("/pool");
        } else{
            alert(nextProps.login.err.msg);
        }
    }
    loginHandler = () => {
        let {userIdMobile,password} = this.state;
        if(!mobileValidator(userIdMobile) &&  !emailValidator(userIdMobile)){
            this.setState({err:'userIdMobile'});
        } else if(password == ''){
            this.setState({err:'password'});
        } else{
            this.props.loginReq({userId:userIdMobile,password});
        }
    }
    enterLoginHandler = (eKey) => {
        if(eKey === 'Enter'){
            this.loginHandler();
        }
    }
    registerHandler = () => {
        this.context.router.history.push("/register");
    }
    render() {
        let {userIdMobile,password,err} = this.state;
        return (
            <div>
                <MenuBar title="Login to App" />
                <Paper className="login-panel" zDepth={3} rounded={false}>
                    <div style={{overflow:'hidden'}}>
                        <TextField
                            type="text"
                            floatingLabelText="Username"
                            fullWidth={true}
                            value={userIdMobile}
                            onKeyPress={e => this.enterLoginHandler(e.key)}
                            errorText={err == 'userIdMobile' ? 'Plese enter valid email/mobile' : ''}
                            onChange={e => {this.setState({userIdMobile : e.target.value})}} />
                        <TextField
                            type="password"
                            floatingLabelText="Password"
                            fullWidth={true}
                            value={password}
                            onKeyPress={e => this.enterLoginHandler(e.key)}
                            errorText={err == 'password' ? 'Plese enter Password' : ''}
                            onChange={e => this.setState({password : e.target.value})} />
                        <RaisedButton label="Login" primary={true} style={{marginTop:10}} onTouchTap={this.loginHandler}/>
                    </div>
                    <div>
                        <p style={{display:'inline-block',fontSize:12,verticalAlign:'middle',marginRight:5}}>Don't have an account, </p>
                        <FlatButton label="Register Now" primary={true} style={{minWidth:60}} labelStyle={{textTransform: 'none',padding:0}} onTouchTap={this.registerHandler} />
                    </div>
                </Paper>
            </div>

        )
    }
}
Login.contextTypes = {
  router: React.PropTypes.object.isRequired
};
function mapStateToProps(state) {
    return { login: state.login.login};
}
export default connect(mapStateToProps,{loginReq})(Login);
