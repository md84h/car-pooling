import React, {Component} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import RaisedButton from 'material-ui/RaisedButton';

const location = [
  'Kormangala',
  'Whitefield',
  'Hebbal',
  'K R Puram',
  'Kalyan Nagar',
  'Marathahalli',
  'M G Road',
  'Shivaji Nagar',
];

export default class Search extends Component{
    state = {
        src:'',
        dest:'',
        err:null
    }
    handleUpdateSrcInput = src => {
        this.setState({src,err:null});
        this.props.hideCar();
    }
    handleUpdateDestInput = dest => {
        this.setState({dest,err:null});
        this.props.hideCar();
    }
    showAvailableCars = () => {
        let {src,dest} = this.state;
        if(location.indexOf(src) == -1 || location.indexOf(dest) == -1) {
            this.setState({err:'Wrong Source/Destination!'});
        } else if(src == dest){
            this.setState({err:'Same Source and Destination!'});
        } else {
            this.props.getCar({src,dest});
        }
    }
    render(){
        const {src,dest,err} = this.state;
        return(
            <div style={{margin:'0 20px'}}>
                <AutoComplete
                    hintText="Source"
                    searchText={src}
                    onUpdateInput={this.handleUpdateSrcInput}
                    filter={AutoComplete.caseInsensitiveFilter}
                    dataSource={location}
                    style={{marginRight:40}}
                    openOnFocus={true}
                />
                <AutoComplete
                    hintText="Destination"
                    searchText={dest}
                    onUpdateInput={this.handleUpdateDestInput}
                    filter={AutoComplete.caseInsensitiveFilter}
                    dataSource={location}
                    style={{marginRight:40}}
                    openOnFocus={true}
                />
                <RaisedButton label="Search" primary={true} onTouchTap={this.showAvailableCars}/>
                {
                    err ?
                        <p className="err">{err}</p>
                    : null
                }
            </div>
        )
    }
}
