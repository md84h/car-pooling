import React, { Component } from 'react';
import { connect } from 'react-redux';
import Toggle from 'material-ui/Toggle';
import MenuBar from '../MenuBar';
import Search from './search';
import MapView from './mapView';
import ListView from './listView';
import Confirm from './confirm';
import {getCookie} from '../../lib/utils';
import {populateCar} from '../../actions/pool';

class Pool extends Component {
    constructor(){
        super();
        this.state = {
            showCar : false,
            showConfirm : false,
            showMap: false
        }
        this.src = '';
        this.dest = '';
    }
    componentCleanup = () =>{
        this.closeWS();
    }
    componentWillMount(){
        if(!getCookie("userId")){
            this.context.router.history.push("/");
        }
    }
    componentDidMount(){
        window.addEventListener('beforeunload', this.componentCleanup)
    }
    componentWillReceiveProps(nextProps){
        this.setState({showCar:true});
    }
    componentWillUnmount(){
        this.componentCleanup();
        window.removeEventListener('beforeunload', this.componentCleanup);
    }
    getCar = (params) =>{
        this.src = params.src;
        this.dest = params.dest;
        this.closeWS();
        this.ws = new WebSocket('ws://localhost:8080');
        this.ws.onopen = () => {
            this.ws.send(params);
            this.intervalId = setInterval(() => {
                this.ws.send(params);
            },2000);
        }
        this.ws.onmessage = res => {
            this.props.populateCar(JSON.parse(res.data));
        }
        this.ws.onclose = () =>{};
    }
    closeWS = () =>{
        if(this.ws && this.ws.readyState !== this.ws.CLOSED){
            clearInterval(this.intervalId);
            this.ws.close();
        }
    }
    hideCar = () => {
        this.closeWS();
        this.setState({showCar:false});
    }
    confirmRide = () => {
        this.closeWS();
        this.setState({showConfirm:true});
    }
    closeDialog = () =>{
        this.src = '';
        this.dest = '';
        this.setState({showConfirm:false,showCar:false});
    }
    handleToggle = (event,val) => this.setState({showMap:val});
    render(){
        const {showCar,showConfirm,showMap} = this.state;
        const {carList} = this.props;
        let cars = [];
        if(carList && carList.data){
            cars = carList.data;
        }
        return(
            <div>
                <MenuBar
                    title="Pick a Ride"
                    logout={true} />
                <div className="container">
                    <Search
                        getCar={this.getCar}
                        hideCar={this.hideCar} />
                    {
                        showCar ?
                            <div style={{padding:'10px 0'}}>
                                <Toggle
                                    label="Map View"
                                    style={{display:'inline-block',width:150,marginLeft:40}}
                                    toggled={showMap}
                                    className="toggle"
                                    onToggle={this.handleToggle}
                                />
                                {
                                    showMap ?
                                        <MapView
                                            cars={cars}
                                            confirmRide={this.confirmRide}
                                            src={this.src}
                                            dest={this.dest} />
                                    :
                                        <ListView
                                            cars={cars}
                                            confirmRide={this.confirmRide}
                                            src={this.src}
                                            dest={this.dest} />
                                }
                            </div>
                        : null
                    }
                </div>
                {
                    showConfirm ?
                        <Confirm closeDialog={this.closeDialog} />
                    : null
                }
            </div>
        )
    }
}
Pool.contextTypes = {
  router: React.PropTypes.object.isRequired
};
function mapStateToProps(state) {
    return { carList: state.car.car};
}
export default connect(mapStateToProps,{populateCar})(Pool);
