import React, {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';

export default class MapList extends Component {
  render() {
      const {data,src,dest} = this.props;
      return (
           <div className={this.props.$hover ? "marker" : "marker hover"}>
              <FontIcon className="material-icons icons" style={{fontSize:30}} color='white'>directions_car</FontIcon>
              <div className="hover_marker">
                  <div className="car-section">
                      <div className="person">
                          <FontIcon className="material-icons icons" style={{fontSize:40}} color="#8d9aa4">person</FontIcon>
                      </div>
                      <div className="details">
                          <p>
                              <span className="name">
                                  {data.name}
                              </span>
                              <span>
                                  {data.waitTime} min(s) Away
                              </span>
                              {
                                  <span className="star">
                                      {data.star} | <FontIcon className="material-icons icons" style={{fontSize:16}} color='#8d9aa4'>star</FontIcon>
                                  </span>
                              }
                          </p>
                          <p>
                              route : <span className="route">{src} to {dest}</span>
                          </p>
                          <p>
                              car : <span className="car-type">{data.car}</span>
                              seat available : <span className="car-type">{data.seat}</span>
                          </p>
                      </div>
                  </div>
                  <p>Click on Car to book</p>
              </div>
           </div>
        );
    }
}
