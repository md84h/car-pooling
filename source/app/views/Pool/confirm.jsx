import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FontIcon from 'material-ui/FontIcon';
import FlatButton from 'material-ui/FlatButton';

export default class Confirm extends Component{
    render(){
        const {closeDialog} = this.props;
        const actions =[
            <FlatButton
                label="Ok"
                primary={true}
                onTouchTap={closeDialog} />
        ]
        return(
            <Dialog
                actions={actions}
                modal={false}
                title={"Successfully Booked"}
                open={true}
                bodyStyle={{textAlign:'center'}}
                onRequestClose={closeDialog}
            >
                <FontIcon className="material-icons icons" style={{fontSize:40}} color="#43A047">check_circle</FontIcon><br />
                Your Ride is booked, Car is on its way, contact the rider.
            </Dialog>
        )
    }
}
