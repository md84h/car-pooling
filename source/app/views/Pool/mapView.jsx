import React, {Component} from 'react';
import GoogleMap from 'google-map-react';
import MapList from './mapList';

export default class MapView extends Component{
    render(){
        const {cars,confirmRide,src,dest} = this.props;
        return(
            <div style={{height:400,width:'100%',marginTop:20}}>
                <GoogleMap
                    bootstrapURLKeys={{key:"AIzaSyADpuemb4PUx2Zfrqvxbhi2X1ja-3JSTGI"}}
                    defaultCenter={{lat: 12.92, lng: 77.62}}
                    defaultZoom={11}
                    hoverDistance={20}
                    onChildClick={confirmRide}
                >
                    {
                        cars.map((data,index)=>(
                            <MapList
                                key={"map"+index}
                                lat={data.location.lat}
                                lng={data.location.long}
                                data={data}
                                src={src}
                                dest={dest}
                            />
                        ))
                    }
                </GoogleMap>
            </div>
        )
    }
}
