import React, {Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

export default class ListView extends Component{
    state = {
        carIndex : null
    }
    selectCar = carIndex => this.setState({carIndex});
    render(){
        const {cars,confirmRide,src,dest} = this.props;
        const {carIndex} = this.state;
        return(
            <div>
                {
                    cars.map((data,index)=>(
                        <div key={"list"+index} className={carIndex == index ? "car-section selected" : "car-section"} onTouchTap={e=>this.selectCar(index)}>
                            <div className="person">
                                {
                                    carIndex == index ?
                                        <FontIcon className="material-icons icons" style={{fontSize:40}} color="white">check_circle</FontIcon>
                                    :
                                        <FontIcon className="material-icons icons" style={{fontSize:40}} color="#8d9aa4">person</FontIcon>
                                }

                            </div>
                            <div className="details">
                                <p>
                                    <span className="name">
                                        {data.name}
                                    </span>
                                    <span>
                                        {data.waitTime} min(s) Away
                                    </span>
                                    {
                                        carIndex == index ?
                                            null
                                        :
                                            <span className="star">
                                                {data.star} | <FontIcon className="material-icons icons" style={{fontSize:16}} color='#8d9aa4'>star</FontIcon>
                                            </span>
                                    }

                                </p>
                                <p>
                                    route : <span className="route">{src} to {dest}</span>
                                </p>
                                <p>
                                    car : <span className="car-type">{data.car}</span>
                                    seat available : <span className="car-type">{data.seat}</span>
                                </p>
                            </div>
                        </div>
                    ))
                }
                {
                    carIndex != null ?
                        <div className="btn">
                            <RaisedButton label="Confirm Ride" primary={true} onTouchTap={confirmRide}/>
                        </div>
                    : null
                }
            </div>
        )
    }
}
