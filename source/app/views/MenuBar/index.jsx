import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FontIcon from 'material-ui/FontIcon';
import FlatButton from 'material-ui/FlatButton';

import {setCookie} from '../../lib/utils';

class MenuBar extends Component {
    logout = () => {
        setCookie("userId","", -1);
        this.context.router.history.push("/");
    }
    render(){
        const {logout,title} = this.props;
        return(
            <AppBar
                title={this.props.title}
                iconElementRight={logout ? <FlatButton label="Log out" style={{color:'white'}} onTouchTap={this.logout}/> : null}
                iconElementLeft={<FontIcon className="material-icons icons" style={{fontSize:40}} color='white'>directions_car</FontIcon>} />
        )
    }
}
MenuBar.contextTypes = {
  router: React.PropTypes.object.isRequired
};
export default MenuBar;
