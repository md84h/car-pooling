import React, { Component } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

import MenuBar from '../MenuBar';
import {registerReq} from '../../actions/register';
import {mobileValidator,emailValidator,getCookie,setCookie} from '../../lib/utils';

class Register extends Component {
    constructor(){
        super();
        this.state = {
            name : '',
            mail : '',
            mobile : '',
            password : '',
            rePassword : '',
            car : '',
            err : ''
        };
        this.loginHandler = this.loginHandler.bind(this);
    }
    componentWillMount(){
        if(getCookie("userId")){
            this.context.router.history.push("/pool");
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.register.success){
            setCookie("userId",nextProps.register.data.userId, 1);
            this.context.router.history.push("/pool");
        } else{
            alert(nextProps.register.err.msg);
        }
    }
    registerHandler(){
        let {name,mail,mobile,password,rePassword,car} = this.state;
        if(name == ''){
            this.setState({err:'name'});
        } else if(!emailValidator(mail)){
            this.setState({err:'mail'});
        } else if(!mobileValidator(mobile)){
            this.setState({err:'mobile'});
        } else if(password == ''){
            this.setState({err:'password'})
        } else if(rePassword != password){
            this.setState({err:'rePassword'})
        } else if(car == ''){
            this.setState({err:'car'})
        } else {
            this.props.registerReq({name,mail,mobile,password,car});
        }
    }
    enterRegisterHandler(eKey){
        if(eKey === 'Enter'){
            this.loginHandler();
        }
    }
    loginHandler = () => {
        this.context.router.history.push("/");
    }
    render() {
        let {name,mail,mobile,password,rePassword,car,err} = this.state;
        return (
            <div>
                <MenuBar title="Register with App" />
                <Paper className="login-panel" zDepth={3} rounded={false} style={{marginTop:30}}>
                    <div style={{overflow:'hidden'}}>
                        <TextField
                            type="text"
                            floatingLabelText="Full Name"
                            fullWidth={true}
                            value={name}
                            errorText={err == 'name' ? 'Plese enter Full Name' : ''}
                            onChange={e => {this.setState({name : e.target.value})}} />
                        <TextField
                            type="text"
                            floatingLabelText="Email ID"
                            fullWidth={true}
                            value={mail}
                            errorText={err == 'mail' ? 'Plese enter valid mail(Required)' : ''}
                            onChange={e => {this.setState({mail : e.target.value})}} />
                        <TextField
                            type="text"
                            floatingLabelText="Mobile Number"
                            fullWidth={true}
                            value={mobile}
                            errorText={err == 'mobile' ? 'Plese enter valid Mobile no(Required)' : ''}
                            onChange={e => {this.setState({mobile : e.target.value})}} />
                        <TextField
                            type="password"
                            floatingLabelText="Password"
                            fullWidth={true}
                            value={password}
                            errorText={err == 'password' ? 'Plese enter Password' : ''}
                            onChange={e => this.setState({password : e.target.value})} />
                        <TextField
                            type="password"
                            floatingLabelText="Re-enter Password"
                            fullWidth={true}
                            value={rePassword}
                            errorText={err == 'rePassword' ? "Password doesn't matches" : ''}
                            onChange={e => this.setState({rePassword : e.target.value})} />
                        <TextField
                            type="text"
                            floatingLabelText="Car Model"
                            fullWidth={true}
                            value={car}
                            errorText={err == 'car' ? "Please enter Car Name" : ''}
                            onKeyPress={e => this.enterRegisterHandler(e.key)}
                            onChange={e => {this.setState({car : e.target.value})}} />
                        <RaisedButton label="Register" primary={true} style={{marginTop:10}} onTouchTap={this.registerHandler.bind(this)}/>
                    </div>
                    <div>
                        <p style={{display:'inline-block',fontSize:12,verticalAlign:'middle',marginRight:5}}>Already have an account? </p>
                        <FlatButton label="Login Now" primary={true} style={{minWidth:60}} labelStyle={{textTransform: 'none',padding:0}} onTouchTap={this.loginHandler} />
                    </div>
                </Paper>
            </div>
        )
    }
}
Register.contextTypes = {
  router: React.PropTypes.object.isRequired
};
function mapStateToProps(state) {
    return { register: state.register.register};
}
export default connect(mapStateToProps,{registerReq})(Register);
