export const URL_PATH = "http://localhost:8080/api/";

export const LOGIN_URL = URL_PATH + "login";
export const LOGIN = "LOGIN";

export const REGISTER_URL = URL_PATH + "register";
export const REGISTER = "REGISTER";

export const POOL_URL = URL_PATH + "pool";
export const POOL = "POOL";
