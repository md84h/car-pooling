import Api from '../api';
import { LOGIN,LOGIN_URL} from '../lib/constant';

export function loginReq(params){
    const request = Api.post(LOGIN_URL,params);
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type:LOGIN,payload:data});
        })
    };
}
