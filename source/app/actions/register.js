import Api from '../api';
import { REGISTER,REGISTER_URL} from '../lib/constant';

export function registerReq(params){
    const request = Api.post(REGISTER_URL,params);
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type:REGISTER,payload:data});
        })
    };
}
